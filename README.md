Jesse Millar's little brother wanted a little utility that would return the first letter of every word in a paragraph, so he created this tool to do just that.

I expanded on it a little bit to also display a mnemonic device for the letters returned.